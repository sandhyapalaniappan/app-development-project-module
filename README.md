### IT1366 - App Development Project Module: TDD with CSharp Room Booking App.

In this project, I developed a Room Booking App using C# while adhering to Test Driven Development (TDD) principles. Throughout the module, I set up xUnit Test Projects and employed Moq and Shouldly for efficient unit testing. I managed project requirements using GitHub Projects and followed the Red-Green-Refactor cycle to ensure thorough testing and integration of new features. By applying TDD principles to real-world code, I wrote easily testable and maintainable code, gaining expertise in architecting reliable ASP.NET Core applications.

### Key Learning Objectives:

- Setting up xUnit Test Projects.
- Utilizing Moq and Shouldly for effective unit testing.
- Managing project requirements using GitHub Projects.
- Implementing the Red-Green-Refactor cycle in TDD.
- Applying TDD principles to real-world code.
- Writing code that is easily testable.
- Understanding best practices and common pitfalls in unit testing.

This module has equipped me to develop robust and maintainable applications, utilising Test-Driven Development to ensure code reliability and functionality.

Thank you so much for your kind support, everyone!